class ImplementationPendingError(NotImplementedError):
    pass


class InexistentEntry(FileNotFoundError):
    pass 

class CorruptionError(Exception):
    pass

class PasswordError(ValueError):
    pass 

class SudoError(ValueError):
    pass 

class StorageViolation(PermissionError):
    pass 

class OperationError(ValueError):
    pass 

class OverwriteError(ValueError):
    pass 

class OperationError(ValueError):
    pass 

class Violation(PermissionError):
    pass 