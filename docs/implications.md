## Nint Resets 

Nint Reset is a bundled extension to nint, whereby one can change the sudo password and/or user passwords to the entries in a nint storage 


### Soft Reset
<hr style="height:2px;margin-top:-1em">
<h6><i>(Candace Mode)</i></h6>

- **Sudo password** for each entry in the database/storage is **reset** to the new password 
- This **does not involve** changing the salt or other **external parameters** 
- The **user password** for entries remain the **same** 

### Hard Reset 
<hr style="height:2px;margin-top:-1em;">
<h6><i>(Isabella Mode)</i></h6>

- **Sudo password, user password and salt** for every entry in the database/password is **reset**
- This **does not affect** other external parameters

### Jade Reset
<hr style="height:2px;margin-top:-1em">
<h6><i>(Jade Mode)</i></h6> 

- **User password** of every entry in the database/storage is **reset** 
- Sudo password, salt of entries are **preserved**
- This **does not affect** external parameters 

### Potential Risks of Resets:      
Losing access through old sudo password and old user passwords. If operation is cancelled midway by the user or due to technical failures or code exceptions, there is no way to figure out which files have been reset and which have not 

### For first time users, it is recommended to keep a backup of the old storage folder before reset. If the operation completes successfully, the backup folder can be deleted. 


<div style="page-break-after: always;"></div>

## Hard Reset/Soft Reset/Jade Reset with backup:
```shell
user@machine$ nint --backup {backup_folder} -d {data_folder}
user@machine$ nint --sudo {sudo_pass} --hard-reset {new_pass} --isabella-config data.csv -d {data_folder} --yes  
user@machine$ rm -rvf {data_folder}
user@machine$ nint --backup {data_folder} -d {backup_folder}
```
<b><u>Using nint --backup over copy/cp gives you the advantage of maintaining date last modified, as well as hardlinking of the aliases