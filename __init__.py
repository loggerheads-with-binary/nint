from argparse import Namespace
import os, sys 
import subprocess

from sqlalchemy import false

"""
For next update, reduce number of times of vals = jihyo(sudo parameters) called. Make a function with lru_cache
"""

from .utils.basicutils import make_link , get_pk , make_dict, startfile, take_consent 

import nint.config as cfg 
from . import Errors 
from . import getpass as gp 
from . import jihyocrypt as jihyo 
  

import logging 
logger = logging.getLogger('nint')
logger.setLevel(logging.INFO)

DB, DBCursor = None , None 

from functools import lru_cache

def __print(holder_func , *args ,  **kwargs):


    if cfg.COLORED:

        import termcolor 
        sep = kwargs.get('sep' , ' ')
        text = sep.join(map(lambda x : x.__str__() , args))
        text = termcolor.colored(text , color = kwargs.get('color' , 'yellow'))
        args = (text , )

    kwargs.pop('sep' , None)
    kwargs.pop('color' , None)
    holder_func(*args , **kwargs)     

eprint = lambda *args, **kwargs : print(*args , file = sys.stderr , **kwargs)
cprint = lambda *args, **kwargs : __print(print , *args, **kwargs)
ceprint = lambda *args , **kwargs : __print(eprint , *args, **kwargs)

@lru_cache
def _compile(expr : str ):
    import re 
    return re.compile(expr)

def regexp(expr : str , item : str):
    import re 
    
    reg = _compile(expr)
    return reg.search(item) is not None

def get_the_damn_database(file : str):

    global DB, DBCursor
    
    if (DB is not None) and (DBCursor is not None) :
        return DB, DBCursor
    
    assert os.path.exists(file), f'Database file {file} does not exist'

    import sqlite3 

    DB = sqlite3.connect(file)
    DB.create_function("REGEXP", 2, regexp)
    DB.row_factory = sqlite3.Row
    DBCursor = DB.cursor()

    return (DB, DBCursor)

def db_reset():

    global DB, DBCursor 

    DB = None
    DBCursor = None 

def obtain_password(prompt : str , mask : gp.SPECIAL_MASKS = None , force_clear : bool = True ):

    if mask is None:
        mask = gp.SPECIAL_MASKS.NO_MASK if cfg.CLEAR_VAR else gp.GP_DEFAULT_MASK

    if force_clear and cfg.CLEAR_VAR:
        mask = gp.SPECIAL_MASKS.NO_MASK

    return gp.getpass(prompt = prompt , colored= cfg.COLORED , mask = mask)

class InteractiveEngine():

    @staticmethod
    def get_query(prompt : str= 'Enter the querystring:: ' , warn : bool = True):

        if warn is True:
            logger.warning("Query list mode is not supported in Interactive Mode")
        
        return obtain_password(prompt , mask = gp.SPECIAL_MASKS.NO_MASK)

class BasicProcessors():

    @staticmethod 
    def query_to_list(query ):

        if isinstance(query  , (list , tuple, set)):
            return tuple(query)

        elif isinstance(query , str ):
            return (query , )
        
        elif isinstance(query  , bytes):

            return (query.decode('utf8' )  , )

        elif query is None:
            return list()

        else:
            raise TypeError(f'BasicProcessors::query_to_list cannot be applied on type {type(query).__name__}')
        
    @staticmethod 
    def query_to_string(query):

        if isinstance(query , str):
            return query 

        elif isinstance(query  , (list , tuple , set )):

            query = tuple(query)

            if len(query) > 1:

                logger.warning(f'Multiple queries provided {query}, which will be reduced to {query[0]}')

            return query[0]

        elif isinstance(query , bytes):
            try:
                return query.decode('utf8')

            except UnicodeDecodeError:
                logger.warning(f'')

                if cfg.UNICODE_SEGFAULT_IGNORE:
                    return query.decode('utf8' , 'ignore')

                else:
                    raise

        elif query is None:
            return ''

        else:
            raise TypeError(f'BasicProcessors::query_to_string cannot be used for type {type(query).__name__}')

    @staticmethod
    def query_to_bytes(query):

        if isinstance(query , bytes):
            return query

        elif isinstance(query , str):
            return query.encode('utf8') 

        elif isinstance(query  , (list , tuple , set )):

            query = tuple(query)

            if len(query) > 1:

                logger.warning(f'Multiple queries provided {query}, which will be reduced to {query[0]}')

                return BasicProcessors.query_to_bytes(query[0])

        else:
            raise TypeError(f'BasicProcessors::query_to_string cannot be used for type {type(query).__name__}')

    @staticmethod
    def __bool_to_int(x):  
        return 1 if x is True else 0 
    
    @staticmethod
    def bool_to_int(x):

        x = bool(x)

        return BasicProcessors.__bool_to_int(x) 

def commit_password(password : str , options : Namespace):
    
    if options.is_text:
        password = BasicProcessors.query_to_string(password)
        password = password[:password.find('\0')]

    if (not options.is_text) and (options.driver not in (cfg.Drivers.PRINT , cfg.Drivers.REDIS , cfg.Drivers.FILE)):

        raise Errors.UnsupportedDriver(f'Unsupported driver {options.driver} for binary password')

    if options.driver == cfg.Drivers.COPY:
        import pyperclip 

        pyperclip.copy(password)
        logger.info('Retrieved password has been copied to the clipboard')

        return None 

    elif options.driver == cfg.Drivers.PRINT:

        if options.is_text:

            logger.info(f'Password will be printed to the terminal')
            sys.stdout.write(password)
            sys.stdout.flush()

            return None 

        else:
            sys.stdout.buffer.write(password)
            sys.stdout.flush()
            logger.info(f'Password typed onto the terminal. Hopefully pull it into a file since it is binary')
            return None 

    elif options.driver == cfg.Drivers.TYPEWRITE:
        
        import time 

        s = time.time()
        logger.info(f'Sleeping for Interval:: {options.typewrite[1]}')

        import pyautogui 
        e = time.time() 

        if (e-s) < options.typewrite[1]:
            time.sleep(options.typewrite[1] - (e-s)) 

        logger.info(f'Typing password')

        s = time.time()
        pyautogui.typewrite(password , interval = options.typewrite[0])
        t = time.time()
        
        logger.info(f'Password successfully typed in {t-s} seconds')
            
    elif options.driver == cfg.Drivers.REDIS:
        
        from redis import Redis 

        if Redis().get(options.redis_key) is not None:

            if not take_consent(f'Do you want to override current value at `{options.redis_key}` in Redis', pc = options.consent):

                logger.critical(f'Redis Set operation cancelled by user')
                return None 

        Redis().set(options.redis_key , password)
        logger.info(f'Password successfully stored in Redis')

    elif options.driver == cfg.Drivers.FILE:

        with open(options.retrieve_output, 'w' if options.is_text else 'wb') as handle:
            handle.write(password)

        logger.info(f'Password successfully written to {options.retrieve_output}')

def nint_standard_retrieve(query : str , password : str , storage : str):       

    query = BasicProcessors.query_to_string(query)
    file = os.path.join(storage , query)

    if not os.path.exists(file):
        raise Errors.InexistentEntry(f'Entry `{query}` does not exist in the given storage `{storage}`')

    import _pickle as pickle 

    with open(file , 'rb') as handle:
        data = pickle.load(handle)

    if password is None:
        password = obtain_password('Enter the user password: ')

    _password =  None 

    for i in range(cfg.MAX_PASS_TRIES):

        try:

            _password = jihyo.dec_b2b(data['enc'] , BasicProcessors.query_to_bytes(password) + data['salt'])
            break 

        except jihyo.PasswordError:

            logger.error(f'The password entered is incorrect. Please try again')
            password = obtain_password('Enter the user password: ')
            continue 

        except jihyo.CorruptionError:
            
            raise Errors.CorruptionError(f'Entry `{query}` has been corrupted')

    if _password is None:
        logger.error('Max tries exceeded')
        raise Errors.PasswordError(f'Incorrect Password Entered for Decryption')

    return BasicProcessors.query_to_string(_password)

def golden_retriever(query : str  , sudopass : str , storage : str):


    query = BasicProcessors.query_to_string(query)
    file = os.path.join(storage , query)

    if not os.path.exists(file):
        raise Errors.InexistentEntry(f'Entry `{query}` does not exist in the given storage `{storage}`')

    if not DBCursor.execute(f"  SELECT EXISTS (SELECT 1 FROM data_holder where default_name IS %s \
                                UNION SELECT 1 from aliases where alias IS %s)" ,
                                (query , query)).fetchone():
    
        logger.warning(f'Entry `{query}` exists in the storage folder, but is not a valid part of the database.')

    entries = DBCursor.execute("SELECT * from sudo").fetchall()
    entries = {entry['name'] : entry['value'] for entry in entries}
    salt , hash_ , nonce = entries['salt'], entries['hash'], entries['nonce']


    vals =  jihyo.key_maker( nonce = nonce , 
                            password = BasicProcessors.query_to_bytes(sudopass)  +  salt, 
                            redundant = True)
                            
    if not vals[-1] == hash_:

        raise Errors.SudoError(f'Incorrect Sudo Password entered')

    import _pickle as pickle 

    with open(file , 'rb') as handle:
        entry_data = pickle.load(handle)

    password = jihyo.dec_b2b(entry_data['sudo-enc'] , password = BasicProcessors.query_to_bytes(vals[1]) + entry_data['salt'])

    return BasicProcessors.query_to_string(password)

@lru_cache 
def get_vals(storage : str):

    get_the_damn_database(os.path.join(storage , cfg.DATABASE_NAME))

    query = 'SELECT * FROM sudo'
    entries = DBCursor.execute(query).fetchall()
    
    return {entry['name'] : entry['value'] for entry in entries}

@lru_cache
def sudo_check(sudopass : str , storage : str):

    entries = get_vals(storage)

    salt , hash_ , nonce = entries['salt'], entries['hash'], entries['nonce']

    vals =  jihyo.key_maker( nonce = nonce , 
                            password = BasicProcessors.query_to_bytes(sudopass)  +  salt, 
                            redundant = True)

    return bool(vals[-1] == hash_)

def multiverify(prompt  : str , mask : gp.SPECIAL_MASKS = gp.SPECIAL_MASKS.MIMI_REVERSE , iters : int = 2 , force_clear : bool = True):

    password,   counter  = ''  , 0 

    while (counter != iters) : 

        if counter == 0 :
            password = obtain_password(prompt , mask, force_clear = force_clear)

        counter = 1 

        if iters == 1:
            return password 

        
        cprint("Kindly verify the entered password" , color = 'green')
        password_ = obtain_password('Enter the password once again: ', mask , force_clear= force_clear)

        if not password_ == password:
            cprint("Entered password is incorrect. Please try the entire process again")
            counter = 0 
            continue 
        
        else:
            counter += 1

            if counter == iters:
                return password 

            continue   

def sudo_make(sudo : bool , sudopass : str , storage : str ):
    
    if sudo == False: 
        raise Errors.SudoError(f'Blankfile mode can only be operated with sudo')

    if sudopass is None:
        sudopass = multiverify('Enter a new sudo password for the storage:: ' , mask = gp.SPECIAL_MASKS.MIMI_REVERSE , force_clear = True , iters = 3 )

    if not os.path.exists(storage):
        logger.info(f'Generating the storage folder `{storage}`')
        os.makedirs(storage)

    salt = os.urandom(cfg.SUDO_SALT_LENGTH)
    nonce , _ , hash_ = jihyo.key_maker(password = BasicProcessors.query_to_bytes(sudopass) + salt , redundant = True)

    file = os.path.join(storage , cfg.DATABASE_NAME)

    import sqlite3 
    DB = sqlite3.connect(file )
    DBCursor = DB.cursor()


    DBCursor.executescript(open(cfg.DEFAULT_SQL_SCRIPT , 'r').read())
    DB.commit()

    query = """
        INSERT INTO sudo 
        (name , value)
        VALUES
        ('salt' , ?),
        ('nonce' , ?),
        ('hash' , ?);
    """
    DBCursor.execute(query , (memoryview(salt) , memoryview(nonce) , memoryview(hash_)))
    DB.commit() 
    DB.close()
    db_reset()

    return True 

def random_password_make(length : int , set_ : str):
    
    if set_ not in cfg.RANDOM_SETS:
        raise ValueError(f'Random set {set_} is invalid')

    charset = getattr(cfg.RandomSets, set_)()
    
    import random
    return ''.join([random.SystemRandom().choice(charset) for i in range(length)])

def generator_filter(options : Namespace):


    if options.ptext is not None:
       
        if options.password_is_text:
            return options.ptext

        else:
            return options.ptext.encode('utf8')
        
    elif options.pfile is not None:
        with open(options.pfile , 'rb') as filehandle:

            if options.password_is_text:
                
                return BasicProcessors.query_to_string(filehandle.read())

            return filehandle.read()

    elif options.prandom is not None:

        if options.password_is_text:

            return random_password_make(options.prandom, options.prandom_set)

        else:
            logger.warning('For binary mode, random set is ignored in password generation')
            return os.urandom(options.prandom)

    elif options.p_diceware is not None:

        from .Diceware import arguments as d_arguments, DriverMain as ddDriverMain

        args = d_arguments(['--no-caps' ,    '-d', options.p_diceware['sep'] ,
                                             '-n', options.p_diceware['n'], 
                                             '-s', options.p_diceware['special']])
        password = ddDriverMain(args)

        if options.password_is_text:
            return password

        return password.encode('utf8')

    else:

        import questionary 
        mode = questionary.select(  'How would you like to feed in the password?' , 
                                    choices = ('Type text' , 'From a file' , 'Diceware' , 'Random')  ).ask()

        if mode == 'Type text':

            options.ptext = multiverify('Enter the password:: ' , iters = 2 , force_clear = True)
            return generator_filter(options)

        elif mode == 'From a file':

            options.pfile = questionary.path('Enter path to file:: ', validate = os.path.isfile).ask()
            options.is_text = questionary.confirm('Is this file a plain text file?')
            return generator_filter(options)

        elif mode == 'Diceware':
            
            options.p_diceware = cfg.diceware_flags()

            options.p_diceware['sep'] = questionary.text('Enter the separator:: ' , validate = lambda x : len(x) == 1 ).ask()

            options.p_diceware['n'] = questionary.text('Enter the number of words:: ' , validate = int).ask()
            options.p_diceware['n'] = int(options.p_diceware['n'])

            if questionary.confirm('Do you want to include special characters?').ask():
                options.p_diceware['special'] = questionary.text("Enter number of special characters:: " , validate = int).ask() 
                options.p_diceware['special'] = int(options.p_diceware['special'])


            return generator_filter(options)

        elif mode == 'Random':

            options.prandom = questionary.text('Enter the length of the password:: ' , validate = int).ask()
            options.prandom = int(options.prandom)

            if questionary.confirm('Would you like a binary random password?').ask():
                options.is_text = False 
                return generator_filter(options)
            
            options.is_text = True 
            options.prandom_set = questionary.select(   'Enter a random set to choose characters from:: ' , 
                                                        choices = cfg.RANDOM_SETS ).ask()
            
            return generator_filter(options)

def holy_cow_generate_me_a_password(options:Namespace, modify_mode : bool = False, pk_entry : int = None ):

    get_the_damn_database(os.path.join(options.storage , cfg.DATABASE_NAME))
    options.query = BasicProcessors.query_to_string(options.query)

    log_text_var = 'Modify' if modify_mode else 'Generate' 

    if not options.sudo:
        raise Errors.StorageViolation(f'Cannot generate a new entry unless in sudo mode. Try again')

     ##By now it has been checked
    if options.userpass is None :
        options.userpass = obtain_password('Enter a user password for this entry:: ')

    if not modify_mode:

        if os.path.exists(os.path.join(options.storage , options.query)):
            raise Errors.StorageViolation((f'Already entry/alias {options.query} exists.'
                'Use modify to replace parameters from the same or update to change the stored password'))

    to_set = generator_filter(options)
    params =  make_dict(options.params)
    logger.info(f'The following parameters will also be appended to the entry: \n{params}')

    vals = get_vals(options.storage)
    vals =  jihyo.key_maker( nonce = vals['nonce'] , 
                            password = BasicProcessors.query_to_bytes(options.sudopass)  +  vals['salt'], 
                            redundant = True)

    salt = os.urandom(cfg.SALT_LENGTH)
    enc = jihyo.enc_b2b(BasicProcessors.query_to_bytes(to_set) , password = BasicProcessors.query_to_bytes(options.userpass) + salt)
    
    
    sudo_enc = jihyo.enc_b2b(BasicProcessors.query_to_bytes(to_set) , password = BasicProcessors.query_to_bytes(vals[1]) + salt)

    params.update({"salt" : salt , 'enc' : enc , 'sudo-enc' : sudo_enc})

    file = os.path.join(options.storage , options.query)

    import _pickle as pickle 
    
    dirname = os.path.dirname(file)

    if not os.path.exists(dirname):
        logger.warning(f'Directory for entry {options.query} does not exist. Creating {dirname}')
        os.makedirs(dirname)

    with open(file , 'wb') as handle:
        pickle.dump(params,  handle)    

    import hashlib 
    hash_ = hashlib.sha256(open(file , 'rb').read()).digest()

    DBCursor.execute('begin')

    if not modify_mode:

        query = f'INSERT INTO data_holder (default_name , sha256) VALUES (? , ?);'
        DBCursor.execute(query , ( options.query, memoryview(hash_) , ))
    
    else:

        query = f'UPDATE data_holder SET sha256 VALUES (%s) WHERE pk is %s'
        DBCursor.execute(query , (memoryview(hash_) , pk_entry))
    
    if take_consent(f'Are you sure you want to make these {log_text_var}ing changes to the database?', pc = options.consent):
        DB.execute('commit')
        logger.warning(f'Database has been modified')

    else:

        DB.execute('rollback')
        logger.critical(f'Database modifications have been nullified, despite file changes have being made. This can lead to errors')


    return os.path.join(options.storage , options.query) 

def __delete_entry(entry : int , storage : str , ayfr : bool = False):

    name = DBCursor.execute(f'SELECT default_name from data_holder WHERE pk is ?' , [entry]).fetchone()['default_name'] 
    aliases = DBCursor.execute(f'SELECT alias from aliases WHERE pk is ?' , [entry]).fetchall()
    aliases = [alias['alias'] for alias in aliases]

    logger.warning(f'Entry::{name} with aliases::{aliases} will be deleted')

    if ayfr is False:
        consent = take_consent("Are you sure you want to delete the above entry?")

        if consent is False:
            return False  

    _ = [os.remove(os.path.join(storage , basename)) for basename in [name] + aliases] 
    logger.info(f'Files of Entry::{name} and all its alias files successfully removed' )

    DBCursor.execute('begin')

    query = f'DELETE FROM aliases WHERE pk is ?;'
    DBCursor.execute(query , (entry ,))

    query = f"DELETE FROM data_holder WHERE pk is ?;"
    DBCursor.execute(query, (entry , )) 
    logger.info(f'Entry::{name} and its aliases::{aliases} successfully removed from the database')
    DBCursor.execute('commit')


    return True 

def get_entries_from_regex(expression : str ):

    assert DB is not None
    assert DBCursor is not None 

    query1 = f"SELECT pk from data_holder WHERE default_name REGEXP '^{expression.__repr__()[1:-1]}$'" 
    query2 = f"SELECT pk from aliases WHERE alias REGEXP '^{expression.__repr__()[1:-1]}$'"

    entries = DBCursor.execute(query1).fetchall()
    entries1 = [entry['pk'] for entry in entries] 
    entries = DBCursor.execute(query2).fetchall()
    entries2 = [entry['pk'] for entry in entries]

    r = set(entries1 + entries2) 
    logger.info(f'A total of {len(r)} entries match regex {expression.__repr__()}')
    return r 

def remove_entries(regexps : list , storage  : str , ayfr : bool = False ):        
    
    all_entries = set()
    _ = [all_entries.update(get_entries_from_regex(expression)) for expression in regexps ] 

    logger.warning(f'A total of {len(all_entries)} entries will be deleted')

    _= [__delete_entry(entry , storage, ayfr) for entry in all_entries]

    return len(all_entries) 

def add_alias(pk : int, alias : str , storage : str ,  entry_name : str, consent : bool  = False, ayfr : bool = False):

    query = "SELECT EXISTS (SELECT 1 from data_holder WHERE default_name is ?) as val"
    r =  DBCursor.execute(query , (alias , )).fetchone()

    if r:
        if r['val'] > 0 :
            raise Errors.OverwriteError(f'Cannot create an alias to an entry when an actual entry `{alias}` already exists')

    query = "SELECT EXISTS (SELECT 1 from aliases WHERE alias is ?) as val"
    r = DBCursor.execute(query , (alias , )).fetchone()

    if r:

        if r['val'] > 0  :

            logger.warning(f'Alias `{alias}` is already a pre-existing alias')
            
            if take_consent('Do you wish to overwrite the existing alias? and modify the database' , pc = ayfr):

                DBCursor.execute('begin')
                query = 'DELETE FROM aliases WHERE alias is ?'
                DBCursor.execute(query , (alias , ))
                DBCursor.execute('commit')

                os.remove(os.path.join(storage , alias))

                logger.critical('Old alias removed')
            
    if take_consent("Do you wish to add the new alias and modify the database?" , pc  = consent|ayfr):
        
        DBCursor.execute('begin')
        query = 'INSERT INTO aliases(pk, alias) VALUES (? , ?)'
        DBCursor.execute(query , (pk , alias))
        DBCursor.execute('commit')
        logger.critical(f'Database modified')
        

        make_link(source = os.path.join(storage , alias) , target = os.path.join(storage , entry_name ))
        logger.info(f'New alias file linked')

        return True 
        
    else:

        logger.info(f'The database modification has been rejected. Operation adding alias {alias} to entry {entry_name} failed')
        return False 

def remove_alias(pk : int , alias : str , storage : str , entry_name : str= None ,  consent : bool = False ):
    
    query = 'SELECT 1 FROM aliases WHERE pk is ? AND alias is ?'

    if entry_name == alias:

        raise Errors.OperationalError(f'Cannot delete main entry instead of aliases')

    elif not DBCursor.execute(query , ( pk , alias  )).fetchone():
        query = 'SELECT 1 FROM data_holder where pk IS ? AND default_name is ? '

        if DBCursor.execute(query , (pk , alias)).fetchone():

            raise Errors.Violation(f"Cannot delete main entry instead of any other aliases")


    if take_consent('Do you really wish to delete these aliases?' , pc = consent):
        
        DBCursor.execute('begin')
        query = "DELETE FROM aliases WHERE pk is ? AND alias is ?"
        DBCursor.execute(query , (pk , alias))
        DBCursor.execute('commit')

        os.remove(os.path.join(storage , alias))
        
        logger.info(f'Operation Successfully Completed')

    else:
        
        logger.error(f'Operation cancelled by user')
        return False 

    return True 

def jijitsu_update_master(params  : dict , query : str , storage : str = cfg.DEFAULT_STORAGE_PATH , consent : bool = False ):

    assert isinstance(params,  dict), f'Params needs to be a dictionary'
    assert isinstance(query , str), f'Query needs to be a string'
    
    file = os.path.join(storage , query)
    assert os.path.exists(file), f'Query does not exist in the storage'

    not_allowed = ('sudopass' , 'enc' , 'sudo-enc' , 'salt' , 'userpass' , 'password')

    t = list(filter(lambda x : x in not_allowed , params.keys()))           

    if len(t) > 0:

        logger.warning(f'You cannot modify parameters::{t} using update/use modify for those')
        raise Errors.OperationError('Invalid operation chosen. Use modify instead')

    import _pickle as pickle 
    
    with open(file , 'rb') as handle:
        loaded_params = pickle.load(handle)

    if not isinstance(loaded_params , dict):
        raise Errors.CorruptionError(f'Entry::{query} has been corrupted')

    loaded_params.update(params)
    pickled_loaded_params = pickle.dumps(loaded_params)

    import hashlib 

    sha256 = hashlib.sha256(pickled_loaded_params).digest()
    pk = get_pk(query)

    DBCursor.execute('begin')
    DBCursor.execute('UPDATE data_holder SET sha256 = ? WHERE pk IS ?' , (sha256 , pk))

    if take_consent('Do you wish to make changes to entry and the database with the hash of the new entry?' , pc = consent):

        DBCursor.execute('commit')

        with open(file , 'wb') as handle:
            handle.write(pickled_loaded_params)

        logger.info(f'Operation Successfully completed')

    else:
        DBCursor.execute('rollback')
        logger.warning(f'Operation cancelled')

    return None 

def change_password_stored(options : Namespace):

    options.query = BasicProcessors.query_to_string(options.query)

    file = os.path.join(options.storage , options.query)
    assert os.path.exists(file), f'Entry to modify does not exist'

    pk = get_pk(options.query)


    import _pickle as pickle 
    with open(file , 'rb') as filehandle:
        params = pickle.load(filehandle)

    assert isinstance(params , dict), f'Data file has been corrupted'
    params.update(options.params)
    options.params = params
    holy_cow_generate_me_a_password(options, True , pk)

    return None 
    
def modify_enc(query , storage, userpass_old: str = None , userpass_new : str = None, sudopass : str = None ):

    assert isinstance(query , str)

    file = os.path.join(storage , query)
    assert os.path.isfile(file), f'File to modify does not exist'

    if sudopass is not None:
        password = golden_retriever(query , sudopass, storage)

    else:

        password = nint_standard_retrieve(query , password , storage)

    if userpass_new is None:
        userpass_new = obtain_password('Enter a new userpassword for this entry' )
    
    import _pickle as pickle 
    with open(file , 'rb') as handle:
        params = pickle.load(handle)
    
    params['enc'] = jihyo.enc_b2b(password , BasicProcessors.query_to_bytes(userpass_new) + params['salt'])

    with open(file , 'wb') as handle:

        pickle.dump(params, handle)

    return True 

def show_entry(entry_name , storage):

    pk, default_name = get_pk(entry_name, get_default_name = True)
    aliases = DBCursor.execute('SELECT alias FROM aliases WHERE pk is ?' , (pk ,))
    aliases = [alias['alias'] for alias in aliases ]

    redhash_params = ('Entry Name::' , 'Aliases::' , 'Time Created::' , 'Time Modified::' , 'Location::')
    lj = len(max(redhash_params , key  = lambda x : len(x))) + 1

    file = os.path.abspath(os.path.join(storage , default_name))

    cprint(f'Location:: '.ljust(lj) , color = 'yellow' , end = '')
    cprint(file , color = 'green')

    cprint('Entry Name:: '.ljust(lj) , color = 'yellow' , end = '')
    cprint(default_name , color = 'green')

    cprint('Aliases:: '.ljust(lj) , color = 'yellow' , end = '')
    
    n = len(aliases)
    if n == 0:
        cprint(0 , color = 'green')

    else:
        cprint( n , '-' , aliases , color = 'green')

    

    from datetime import datetime
    time_created = datetime.fromtimestamp(os.stat(file).st_ctime)
    time_modified = datetime.fromtimestamp(os.stat(file).st_mtime)
    
    cprint('Time Created:: '.ljust(lj) , color = 'yellow' , end = '')
    cprint(datetime.strftime(time_created , '%d-%m-%Y %H:%M:%S') , color = 'green')

    cprint("Time Modified:: ".ljust(lj) , color = 'yellow' , end = '')
    cprint(datetime.strftime(time_modified , '%d-%m-%Y %H:%M:%S') , color = 'green')

    import _pickle as pickle 

    with open(file , 'rb') as handle:
        vals = pickle.load(handle)

    params = set(vals.keys()).difference({'salt' , 'enc' , 'sudo-enc'})
    lj = len(max(params , key = len )) + 3 

    for param in params:

        cprint( f'{param}:: '.ljust(lj) , color = 'white' , end = '') 
        cprint(vals[param] , color = 'cyan' )

    return True 

def __rename(old , new, storage, consent : bool = False):

    new_file = os.path.join(storage , new)
    old_file = os.path.join(storage , old)

    assert os.path.exists(old_file), f'Entry {old} does not exist in storage'
    assert not os.path.exists(new_file) , f'Entry {new} already exists in storage'

    if take_consent(f'Are you sure you wish to rename `{old}` with `{new}`' , pc = consent):

        os.rename(old_file , new_file)
        logger.info(f'Renamed file from {old} to {new}')

        DBCursor.execute('begin')
        _ = [ DBCursor.execute(query , (new , old)).fetchone() for query in \
            ('UPDATE data_holder SET default_name = ? WHERE default_name = ?' , 
            'UPDATE aliases SET alias = ? WHERE alias = ?')
            ]

        DBCursor.execute('commit')

        logger.info(f'Corresponding database edit successfully completed')
        return True 

def soft_reset(storage , old , new):

    assert sudo_check(old , storage)

    salt = os.urandom(cfg.SUDO_SALT_LENGTH)
    
    nonce , _ , hash_ = jihyo.key_maker(password = BasicProcessors.query_to_bytes(new) + salt , redundant = True)

    query = """INSERT INTO sudo VALUES ('salt' , ?),('nonce' , ?),('hash', ?)"""
    DBCursor.execute('begin')
    DBCursor.execute(query , (memoryview(salt) , memoryview(nonce) , memoryview(hash_)))
    DBCursor.execute('commit')
    logger.info('Sudo password successfully changed')

    return True 

def voodoo_reset(storage , old , new , hard_mode : bool = True ):

    global DBCursor 

    import _pickle as pickle 

    counter = 0 
    total = DBCursor.execute('SELECT COUNT(*) as [cnt] from data_holder').fetchone()['cnt']
    
    machinery = DBCursor.execute('SELECT pk , default_name FROM data_holder')
    val = machinery.fetchone()
    
    while val:

        name , pk = machinery['default_name'], machinery['pk']
        aliases = DBCursor.execute('SELECT alias FROM aliases WHERE pk is ?' , (pk)).fetchall()
        aliases = {alias['alias'] for alias in aliases}

        logger.info(f"Resetting user password for entry `{name}`")
        
        if len(aliases) > 0 :
            logger.info(f"Known aliases: {aliases}")

        if hard_mode:
            password = multiverify('Provide a new user password:: ' , force_clear = True , iters = 2  )
        
        file = os.path.join(storage , name)

        with open(file , 'rb') as handle:
            params = pickle.load(handle)

        ptext = jihyo.dec_b2b(params['sudo-enc'] , password = BasicProcessors.query_to_bytes(old) + params['salt']) 
        
        if hard_mode:
        
            params['salt'] = os.urandom(len(params['salt']))        
            params['enc'] = jihyo.enc_b2b( ptext , password =  BasicProcessors.query_to_bytes(password) + params['salt'] )        
        
        params['sudo-enc'] = jihyo.enc_b2b(ptext , password =  BasicProcessors.query_to_bytes(new) + params['salt'])

        with open(file , 'wb') as handle:
            pickle.dump(params , handle)

        logger.info(f'File corresponding to entry `{name}` has been updated')

        import hashlib 
        sha256 = hashlib.sha256(open(file , 'rb').read()).digest()

        DBCursor.execute('begin')
        DBCursor.execute('UPDATE data_holder SET sha256 = ? WHERE pk = ?' , (memoryview(sha256) , pk))
        DBCursor.execute('commit')

        val = machinery.fetchone()
        
        counter += 1 
        logger.info(f'Completed {counter}/{total} entries hard reset')

def DriverMain(options : Namespace):

    global logger 

    if options.action is None:
        raise Errors.OperationError(f'No action has been chosen to execute')

    if options.action == cfg.Actions.VERSION: 
        sys.stdout.write(f'{cfg.__version__}\n')
        return None 

    elif options.action == cfg.Actions.USAGE:

        with open(cfg.HELP_TEXT , 'rb') as filehandle:

            sys.stdout.buffer.write(filehandle.read())
        return None 

    elif options.action == cfg.Actions.BLANKFILE:

        ##Create storage system 
        if os.path.exists(options.storage):

            if os.path.isfile(options.storage):

                raise Errors.StorageViolation(f'Storage parameter provided is a file, not a directory')

            if len(os.listdir(options.storage)) > 0 :
            
                raise Errors.StorageViolation(f"New storages can only be created in empty directories")


        #sudo_assert(options.sudo , options.sudopass , options.storage)
        
        sudo_make(options.sudo, options.sudopass , options.storage)
        return None 

    elif options.action == cfg.Actions.LIST:
        
        args = [arg.strip() for arg in BasicProcessors.query_to_list(options.query)]
        subprocess.call(['lsd' , *args ] , cwd = options.storage)
        return None

    ##TODO: Complete these functionalities 
    elif options.action in (cfg.Actions.SOFT_RESET , cfg.Actions.HARD_RESET, cfg.Actions.JADE_RESET):

        if not options.sudo:
            raise Errors.SudoError(f'{options.action} cannot be done without sudo mode')

        get_the_damn_database(os.path.join(options.storage , cfg.DATABASE_NAME))
        
        logger =            logging.getLogger('isabella') if options.action == cfg.Actions.HARD_RESET else \
                            (logging.getLogger('candace') if options.action == cfg.Actions.SOFT_RESET else \
                            logging.getLogger('jade')
                            )

        if not options.consent:

            import questionary 
            cnf = questionary.confirm(f'Do you realize the implications and potential risks of {options.action} reset\n\
                    /and have you preferably stored a backup?').ask()

            if not cnf:

                logger.warning("Please wait for the implications file to load")
                startfile(os.path.join(cfg.PROG_PATH , 'docs' , 'implications.pdf'))
                logger.warning("Kindly read through the implications and other methods before proceeding")

            cnf = questionary.confirm('Do you still want to continue?').ask()
            if not cnf:

                logger.critical(f'{options.action} cancelled by user')
                return None 

        else:

            logger.warning('It is assumed you understand the implications and potential risks of Hard and Soft Resets')

        if (options.newpass is None) or (options.action == cfg.Actions.JADE_RESET):

            logging.warning('A sudo password is the most critical component of the program, be careful while setting it. \
                This will be rendered in MIMI-REVERSE mode, make sure no one else is around you.')
            
            options.newpass = multiverify("Enter a new sudo password for your storage: " ,  mask = gp.SPECIAL_MASKS.MIMI_REVERSE , force_clear = False )

        if not options.action == cfg.Actions.JADE_RESET:
            t = soft_reset(options.storage , options.sudopass , options.newpass )

            if not t : 

                raise Errors.OperationError('Reset Operation failed since soft reset operation was cancelled by user')

        if options.action == cfg.Actions.SOFT_RESET:
            voodoo_reset(options.storage , options.sudopass, options.newpass , hard_mode = False)

        elif (options.action == cfg.Actions.HARD_RESET ) : 

            raise Errors.ImplementationPendingError()

        elif options.action == cfg.Actions.JADE_RESET :

            raise Errors.ImplementationPendingError()

    elif options.action == cfg.Actions.IMPORT:

        if not options.sudo:
            raise Errors.SudoError(f'Import Operation cannot be performed without sudo access')

        if os.path.isfile(options.storage):
            raise NotADirectoryError('Storage directory given is already an existing file')

        if not os.path.exists(options.storage):
            
            if not options.import_mode is not cfg.IMPORT_MODE.NEW:
                raise Errors.StorageViolation(f'Storage directory given does not exist, thus append cannot be used')
            
            os.makedirs(options.storage)
            
        elif len(os.listdir(options.storage))> 0 :
            
            if options.import_mode is not cfg.IMPORT_MODE.APPEND:
                
                raise Errors.StorageViolation(f'Storage directory given is not empty, and import mode is not append')
            
        if options.import_mode is cfg.IMPORT_MODE.NEW:
            sudo_make(options.sudo, options.sudopass , options.storage)
            
        
        file = os.path.join(options.storage , cfg.DATABASE_NAME)
        get_the_damn_database(file)    
        vals = get_vals(options.storage)
        
        options.sudopass = BasicProcessors.query_to_bytes(options.sudopass)
        jihyo_salt = jihyo.key_maker(nonce = vals['nonce'] , password =  + vals['salt'] )[1]
                
        from .utils import ImportMachine as importmachine
        
        importmachine.functions['take_consent'] = take_consent 
        importmachine.functions['table_name'] = options.sql_table_name
        importmachine.functions['multiverify'] = multiverify 
        importmachine.functions['obtain_password'] = obtain_password
        importmachine.functions['param_action'] = options.expand_import_params
        importmachine.functions['salt_size'] = cfg.SALT_LENGTH
        importmachine.functions['startfile'] = startfile 
        importmachine.functions['prog_path'] = cfg.PROG_PATH
        
        return importmachine.__import(options._import , options.storage, jihyo_salt , file, DB, DBCursor , options.import_mode )
        
    elif options.action == cfg.Actions.EXPORT:

        if not options.sudo : 
            raise Errors.SudoError(f'Export operation cannot be completed without sudo access')

        vals = get_vals(options.storage)    
        options.sudopass = BasicProcessors.query_to_bytes(options.sudopass)
        jihyo_salt = jihyo.key_maker(nonce = vals['nonce'] , password =  + vals['salt'] )[1]

        from .utils import ExportMachine as exportmachine
        
        exportmachine.TABLE_NAME = options.sql_table_name
        return exportmachine.__export(  options._export , 
                                        options.storage,
                                        os.path.join(options.storage , cfg.DATABASE_NAME) , 
                                        jihyo_salt 
                                    )    

    elif options.action == cfg.Actions.BACKUP:

        if options.sudo:
            logger.warning("Nint4 allows for backups without sudo mode")

        from .utils import BackupMachine

        if os.path.isdir(options.backup_dir):

            logger.critical(f'A directory {options.backup_dir} already exists. All the internal files will be deleted if you choose to move forward')

            if not take_consent('Should the forced back up be performed?' , pc = options.consent):

                logger.warning(f'Backup operation cancelled on user instruction')
                return False 

        get_the_damn_database(os.path.join(options.storage , cfg.DATABASE_NAME))
        BackupMachine.backup(options.storage , options.backup_dir , DB, DBCursor )
        return True  

    if options.query is None:

        options.query = InteractiveEngine.get_query()

    if options.action == cfg.Actions.RETRIEVE:

        if options.sudo:
            get_the_damn_database(os.path.join(options.storage , cfg.DATABASE_NAME ))
            password = golden_retriever(options.query , options.sudopass, options.storage )

        else:
            logger.warning(f'For non sudo retrievals, use nint.r for enhanced speed and performance')
            password = nint_standard_retrieve(options.query , options.userpass , options.storage )


        commit_password(password , options)
        return None 

    elif options.action == cfg.Actions.GENERATE:
        
        assert options.sudo is True, f'Generation of a new entry can only be done in sudo mode'
        holy_cow_generate_me_a_password(options)

    elif options.action == cfg.Actions.REMOVE:

        regexps = BasicProcessors.query_to_list(options.query)
        get_the_damn_database(os.path.join(options.storage , cfg.DATABASE_NAME))
        remove_entries(regexps , options.storage , options.ayfr)

    elif options.action == cfg.Actions.ADD_ALIAS:

        options.query = BasicProcessors.query_to_string(options.query)
        options.aliases = BasicProcessors.query_to_list(options.aliases)

        get_the_damn_database(os.path.join(options.storage, cfg.DATABASE_NAME))
        pk, name= get_pk(options.query , get_default_name = True)
        _ = [add_alias(pk , alias, options.storage, name, options.consent , options.ayfr  ) for alias in options.aliases]

    elif options.action == cfg.Actions.RM_ALIAS:

        options.query = BasicProcessors.query_to_string(options.query)
        options.aliases = BasicProcessors.query_to_list(options.aliases)

        get_the_damn_database(os.path.join(options.storage, cfg.DATABASE_NAME))
        pk , name = get_pk(options.query , get_default_name = True)
        _ = [remove_alias(pk , alias , options.storage , name , options.consent) for alias in options.aliases]

    elif options.action == cfg.Actions.RENAME:

        get_the_damn_database(os.path.join(options.storage , cfg.DATABASE_NAME))
        
        options.query = BasicProcessors.query_to_list(options.query)
        options.newnames = BasicProcessors.query_to_list(options.newnames)

        if len(options.query ) != len(options.newnames):

            raise Errors.OperationError(f'There are {len(options.query)} entries to rename, but only {len(options.newnames)} \
            /newnames, causing this ambiguity')

        for query , newname in zip(options.query , options.newnames):

            __rename(query , newname , options.storage , options.consent)
    
    elif options.action == cfg.Actions.UPDATE:

        options.params = make_dict(options.params)

        get_the_damn_database(os.path.join(options.storage,  cfg.DATABASE_NAME))
        return jijitsu_update_master(options.params, BasicProcessors.query_to_string(options.query) , options.storage , options.consent)

    elif options.action == cfg.Actions.MODIFY:

        raise Errors.ImplementationPendingError()

    elif options.action == cfg.Actions.SHOW:
        
        get_the_damn_database(os.path.join(options.storage , cfg.DATABASE_NAME))
        _ = [show_entry(query , options.storage) for query in BasicProcessors.query_to_list(options.query)]

def filtration(options: Namespace):

    global logger

    cfg.COLORED = options.colored 
    cfg.CLEAR_VAR = options.clear 

    if options.query_string is None:
        if len(options.query_list) == 0 :
            options.query = None 

        else:
            options.query = options.query_list 

    else:
        options.query = options.query_string 

    delattr(options,  'query_string')
    delattr(options , 'query_list')

    if options.query is not None:
        if cfg.DATABASE_NAME in options.query:
            logger.error(f"It is by default not acceptable to use {cfg.DATABASE_NAME} as a query due to file clashes")

    if (options.blankfile is not None):
        
        options.storage = options.blankfile 
        options.action = cfg.Actions.BLANKFILE
        options.sudopass = None

    if options._backup is not None:

        options.action = cfg.Actions.BACKUP

        if options._backup is True:
            logger.info("Backing up to `{options.storage}.backup`")
            options.backup_dir =  f'{options.storage}.backup'

        else    :
            options.backup_dir = options._backup

    elif options._export is not None:

        options.action = cfg.Actions.EXPORT

    if options._import is not None:

        options.action = cfg.Actions.IMPORT

    else:

        delattr(options, 'import_mode')

    if (options.sudo is not False ):
        
        if options.action == cfg.Actions.BLANKFILE:
            pass
        
        elif options.action == cfg.Actions.IMPORT and options.import_mode == cfg.IMPORT_MODE.NEW:
            pass   
        
        if not os.path.isfile(os.path.join(options.storage , cfg.DATABASE_NAME)):

            raise Errors.StorageViolation(f'Storage folder provided is invalid as it does not have a database inside')

        if options.sudo == True:

            options.sudopass = obtain_password(prompt = 'Enter the Sudo Password: ')

        else:
            options.sudopass = options.sudo 
            options.sudo = True 

        if sudo_check(options.sudopass , options.storage ) == False:
            
            sudo_ = False 
            for i in range(cfg.MAX_SUDO_TRIES -1):
                
                logger.error(f'Incorrect Sudo Password entered is incorrect. Please try again')
                options.sudopass = obtain_password

                if sudo_check(options.sudopass , options.storage) == True:
                    sudo_ = True    
                    break 
            
            if sudo_ == False:
                raise Errors.SudoError("Sudo Password Entered is Incorrect. Operation will be terminated")
                sys.exit(0xff)
            
            del sudo_
       

        logger = logging.getLogger('nint-sudo') 

    if options.typewrite is not None:
       
        options.driver = cfg.Actions.TYPEWRITE

        if len(options.typewrite) == 0:
            options.typewrite = cfg.TYPEWRITER_DEFAULTS
        
        elif len(options.typewrite) > 2 :

            raise AttributeError("Can only specify a maximum of two arguments for typewriter parameters")
    
        elif len(options.typewrite) == 1 : 

            options.typewrite.append(cfg.TYPEWRITER_DEFAULTS[1])

    else:

        delattr(options , 'typewrite')

        if options.redis_args is not None:

            options.driver = cfg.Actions.REDIS 
            options.redis_variable = options.redis_args.copy() 

    delattr(options , 'redis_args')

    if options.retrieve_output is not None:

        if options.retrieve_output is True:

            options.retrieve_output = os.path.abspath('nint.txt' if options.is_text else 'nint.bin')
            logger.warning(f'Setting retrieve output file as {options.retrieve_output}')

        options.driver = cfg.Drivers.FILE
    else:
        delattr(options , 'retrieve_output')


    if options.aliases is not None:
        options.action = cfg.Actions.ADD_ALIAS   

    elif options.r_aliases is not None:
        options.aliases = options.r_aliases.copy()
        options.action = cfg.Actions.RM_ALIAS
    
    delattr(options , 'r_aliases')

    if options.isabella is not None:

        if options.isabella == cfg.Actions.HARD_RESET:
            options.newpass = None

        else:
            options.newpass = options.isabella.copy()

        options.action = cfg.Actions.HARD_RESET

    elif options.candace is not None:

        if options.candace == cfg.Actions.SOFT_RESET:
            options.newpass = None

        else:
            options.newpass = options.candace.copy()

        options.action = cfg.Actions.SOFT_RESET

    delattr(options , 'candace')
    delattr(options,  'isabella')

    if options.newnames is not None:
        options.action = cfg.Actions.RENAME
    
    else:
        del options.newnames 


    return options 

if __name__ == '__main__':

    raise Exception("Run nint.__main__ instead of nint.__init__ as a module")
