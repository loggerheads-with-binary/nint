from argparse import ArgumentParser, Namespace 
import nint  
import nint.config as cfg 
import logging 
import os 

def arguments(args : list = None):

    parser = ArgumentParser(prog = "nint" , description = "A sophisticated commandline tool to create, maintain and edit passwords in an encrypted manner on your local system. Inspired from UNIX Pass" )

    parser.add_argument('-q' , '--query' , action = 'store' , dest = 'query_string' , default = None ,  
                        help = 'Query to work on')
    
    parser.add_argument(dest = 'query_list' , metavar = 'Query' , action = 'store' , nargs = '*' , default = list()  , 
                        help = 'Query/ies to work on')

    t1 = parser.add_mutually_exclusive_group()

    t1.add_argument('-v' , '--version' , action = 'store_const' , const = cfg.Actions.VERSION , default = None ,  dest = 'action' , 
                    help = "Obtain NINT version details")

    t1.add_argument('-U' , '--usage' , action = 'store_const' , const = cfg.Actions.USAGE , default = None , dest = 'action' , 
                    help = "Obtain NINT Usage information")

    t1.add_argument('--touch', '--blankfile' , nargs = '?' , type = os.path.abspath, const = os.getcwd() , default = None , dest = 'blankfile' , 
                    help = "Create a new nint database in an existing folder or new one")

    t1.add_argument('-r' , '--retrieve' , '--obtain' , action = 'store_const' , const = cfg.Actions.RETRIEVE , dest= 'action' ,  default = None , 
                    help = 'Retrieve a password entry')

    t1.add_argument('-g' , '--generate' , '--create' , action = 'store_const' , dest= 'action' , const = cfg.Actions.GENERATE , default = None , 
                    help = 'Generate a new entry' )

    t1.add_argument('--add-alias', '--add-aliases' , action = 'store' , nargs=  '+' , default = None , dest = 'aliases')
    t1.add_argument('--rm-alias' , '--rm-aliases' , action = 'store' , nargs= '+' , default = None , dest = 'r_aliases')

    t1.add_argument('-m' , '--modify' , action = 'store_const' , const = cfg.Actions.MODIFY, default = None , dest = 'action'  )

    t1.add_argument('-s' , '--show' , action = 'store_const' , const = cfg.Actions.SHOW , default = None , dest = 'action'  )

    t1.add_argument('--ls' , '--lsd' , '--list' , action = 'store_const', default = None , const = cfg.Actions.LIST , dest = 'action')

    t1.add_argument('--rm' , '--remove' , '--del' , action = 'store_const' , dest = 'action' , default = None , const = cfg.Actions.REMOVE)

    t1.add_argument('--update' , action = 'store_const' , const = cfg.Actions.UPDATE , dest = 'action' , default = None )

    t1.add_argument('--soft-reset' , '--candace' , '--candace-reset' ,  dest = 'candace' , 
                    default = None , const = cfg.Actions.SOFT_RESET ,  nargs = '?')

    t1.add_argument('--hard-reset' , '--isabella' , '--isabella-reset' , dest = 'isabella' , 
                    default = None , const = cfg.Actions.HARD_RESET , nargs = '?' )

    t1.add_argument('--rename' , '--rename-entries' , dest = 'newnames' , default = None , nargs = '+' )

    t1.add_argument('--feed' , '--import' , '--process' , dest = '_import' , default = None , const = True , nargs = '?' , 
    type = os.path.abspath)

    t1.add_argument('--export' , '--freeze' , dest = '_export' , default = None, const = True , nargs = '?', 
    type = os.path.abspath)
    
    parser.add_argument('--sql-table-name' , dest = 'sql_table_name' , default = 'nint'  )
    parser.add_argument('--expand-export-params' , action = 'store_true')
    parser.add_argument('--action-import-params' , choices = ('import' , 'ignore' , 'ask') , default = 'ask')
    parser.add_argument('--import-mode' , choices = ('new' , 'append') , type = lambda x : cfg.IMPORT_MODE[x.upper()])

    t1.add_argument('--backup' , '--backup-db' , dest = '_backup' , default = None , const = True , nargs = '?' ,type = os.path.abspath)
    
    
    parser.add_argument('--isabella-config' , '--hard-reset-config' , nargs = '?' , dest = 'isabella_config' , 
                        default = None, const = True , type = os.path.abspath )
    ##json, yaml, csv, excel, pickle  

    parser.add_argument('--large-reset' , dest = 'isabella_large_reset' , action = 'store_true')

    c = parser.add_mutually_exclusive_group()

    c.add_argument('--not-colored' , '--no-color' , dest = 'colored' , action = 'store_false' , default = cfg.COLORED , 
                        help = "Do not color any text, by default color::{nint.DEFAULT_COLOR}")

    c.add_argument('--colored' , '--color' , dest = 'colored', action = 'store_true' , default = cfg.COLORED ,
                       help =  "Color text by default, by default color::{nint.DEFAULT_COLOR}")

    d = parser.add_mutually_exclusive_group()

    d.add_argument('-P' , '--print' , dest = 'driver' , default = cfg.DEFAULT_DRIVER , const = cfg.Drivers.PRINT , action = 'store_const' , 
                    help = 'Print the password obtained onto the screen' )

    d.add_argument('-c' , '-C' , '--copy' , dest = 'driver' , default = cfg.DEFAULT_DRIVER , const = cfg.Drivers.COPY , action = 'store_const' , 
                    help = "Copy the password obtained onto the clipboard")

    d.add_argument('-t' , '--typewrite' , dest = 'typewrite' , 
                    default = None , metavar = '<wait> <interval>' , nargs = '*' ,   
                    help = f'Typewrite the password onto the screen. \
                            -t <time in secs to wait(default {float(cfg.TYPEWRITER_DEFAULTS[0])})> \
                                <time in secs interval(default {float(cfg.TYPEWRITER_DEFAULTS[1])})>', type = float )
    
    d.add_argument( '--redis' , '--redis-server' , dest = 'redis_args' , 
                    default = None , const = cfg.REDIS_DEFAULT , nargs = '?' , help = f'Store password onto the Redis Cache.\
                    Optional argument: Variable Name, defaults to {cfg.REDIS_DEFAULT}' )

    d.add_argument('-o' , '--output-file' , dest = 'retrieve_output' , default = None , const = True , nargs = '?' ,
                    type = os.path.abspath , help = 'Retrieve output into a file')

    parser.add_argument('-u' , '--upass' , '--userpass' , dest = 'userpass' , default = None , help = 'User level password for the entry' )

    parser.add_argument('--ptext' , dest = 'ptext' , default = None , type = str , help = '(To store) password in the form of text')
    parser.add_argument('--pfile' , dest = 'pfile' , default = None , type = os.path.abspath , help = '(To store) password from a file')
    parser.add_argument('--random' , dest = 'prandom' , default = None , type = int , const = cfg.DEFAULT_RANDOM_LENGTH , nargs = '?', help = '(To store) a randomly generated text password')
    parser.add_argument('--random-set' , dest = 'prandom_set' , default = 'alpha' , choices =('alpha' , 'num' , 'alphanum' , 'all' , 'numsym' , 'alphasym')  , 
    help = 'Random set to choose from.' )
    parser.add_argument('--diceware' , dest = 'p_diceware' , default = None , type = cfg.diceware_flags , help = '(To store) password in the form of a diceware passphrase')

    parser.add_argument('-b' , '--binary' , dest = 'password_is_text' , action = 'store_false' )
    parser.add_argument('--clear' , dest = 'clear' , action = 'store_true')

    parser.add_argument('--params' , dest = 'params' , action = 'store' , nargs = '+' , default = list() ,   
                        help = 'Parameters for the entry during generation/modification')

    parser.add_argument('-S' , '-d' , '--storage' , type = os.path.abspath , default = cfg.DEFAULT_STORAGE_PATH )

    parser.add_argument('--sudo' , '--osudo' , '--SUDO' , nargs = '?' , default = False , const = True , dest = 'sudo')

    parser.add_argument('-y' , '--yes' , dest = 'consent' , action = 'store_true')

    parser.add_argument('--ayfr' , '--assume-yes-for-removal' , dest = 'ayfr' , action = 'store_true')

    if args is None:
        return parser.parse_args()

    else: 
        return parser.parse_args(args)

if __name__ == '__main__':

    import pretty_traceback , coloredlogs

    pretty_traceback.install()
    coloredlogs.install(fmt = "[%(name)s] %(asctime)s %(levelname)s : %(message)s" , level = logging.DEBUG)

    args = nint.filtration(arguments())
    nint.DriverMain(args)           ##Run driver function 
    
    