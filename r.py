from argparse import ArgumentParser as ArgumentParser_src, Namespace
import os
import logging
import sys

import annautils.jihyocrypt as jihyo
from annautils.getpass import getpass



PROG_PATH = os.path.dirname(os.path.realpath(__file__))
DEFAULT_STORAGE = os.path.join(PROG_PATH , '.n4_storage')


logger = logging.getLogger("nint.r")
logger.setLevel(logging.DEBUG)


MAX_RETRIES = 3

DEFAULT_RETRIEVE_FLAGS = ( "-r" , "--retrieve" , "--obtain" , "--procure" )

DEFAULT_USER_PASSWORD_FLAGS = ('-u' , '--upass' , '--user-pass' )
DEFAULT_SUDO_PASSWORD_FLAGS = ('--sudo' , '--sudo-pass')

DEFAULT_QUERY_FLAGS = ('-q' , '--query' , '--entry' , '--id')

DEFAULT_STORAGE_DEST_FLAGS = ('-s' , '-d' ,'--storage' ,  '--data-box')

DEFAULT_DRIVER = 'copy'
DEFAULT_COPY_FLAGS = ('-c' , '--copy')
DEFAULT_PRINT_FLAGS = ('-p' , '--print' )
DEFAULT_TYPE_FLAGS = ('-t' , '--type' , '--typewrite' , '--type-write')
DEFAULT_BINARY_PASS_FLAGS = ('-b' , '--binary' , '--no-unpad')
DEFAULT_TYPE_OPTS = (4 , 0.1)
REDIS_DEFAULT_VARIABLE = 'NINT'



class ArgumentParser(ArgumentParser_src):

	def print_help(self , buffer = sys.stdout):

		buffer.write(self.description)
		#buffer.write(self.usage)
		buffer.write('\n')

		with open(os.path.join(PROG_PATH , 'nint.r.txt') , 'r') as filehandle:

			buffer.write(filehandle.read())

		buffer.write('\n')
		buffer.write(f'Defaults:\n\tProgram Location:{PROG_PATH}\n\tJihyoCrypt Location: {jihyo.__file__}\n\n')
		buffer.write(f'Default Configuration:\n\tStorage: {DEFAULT_STORAGE}\n\tMax Retries: {MAX_RETRIES}\n\tDefault Driver: {DEFAULT_DRIVER}')
		buffer.write(f'\n\tDefault Typewrite: \tDelay: {DEFAULT_TYPE_OPTS[0]}s\tInterval: {DEFAULT_TYPE_OPTS[-1]}s')
		buffer.write(f'\n\tDefault Redis: \tVariable: {REDIS_DEFAULT_VARIABLE}')
		buffer.write('\n')

		buffer.flush()
		sys.exit(0)

class EntryError(ValueError):
	pass

def arguments(args : list = None ):

	parser = ArgumentParser(prog = "nint.r" ,
							description = "Nint Default Retrieval: A light script for only password retrieval purposes" )

	parser.add_argument(dest = "query_list" , nargs = '?' , const = None , default = None )

	parser.add_argument(*DEFAULT_QUERY_FLAGS , dest = "query" , default = None , action = 'store' )

	parser.add_argument(*DEFAULT_RETRIEVE_FLAGS , dest = "blank" , action = 'store_true' )

	ss = parser.add_mutually_exclusive_group()

	ss.add_argument(*DEFAULT_COPY_FLAGS , dest = 'driver' , action = 'store_const' , const = 'copy' , default = None )

	ss.add_argument(*DEFAULT_PRINT_FLAGS , dest = 'driver' , action = 'store_const' , const = 'print' , default = None  )

	ss.add_argument(*DEFAULT_TYPE_FLAGS , dest = 'typewrite' , nargs = '*' , metavar = '' , default = None )

	ss.add_argument('--redis' , '--redis-server' , dest = 'redis' , nargs = '?' , default = None , const = True , 
	help = "Store the password onto a Redis variable") 

	parser.add_argument(*DEFAULT_USER_PASSWORD_FLAGS , dest = 'upass' , action = 'store' , 	metavar = "***" , default = None  )

	parser.add_argument(*DEFAULT_SUDO_PASSWORD_FLAGS , dest = 'sudo' , nargs = '?' , default = False , const = True , metavar = '***' )

	parser.add_argument(*DEFAULT_STORAGE_DEST_FLAGS , dest = 'storage' , type = os.path.abspath ,
						metavar = '<storage>' , default = DEFAULT_STORAGE )

	parser.add_argument('--clear' , dest = 'clear' ,action = 'store_true' )

	parser.add_argument(*DEFAULT_BINARY_PASS_FLAGS , dest = 'password_is_text' , action = 'store_false')

	if args is None :
		return parser.parse_args()

	return parser.parse_args(args)

def DriverMain(opts : Namespace):

	if opts.query is None:
		raise EntryError(f'Query Cannot be None for Retrieval')

	file = os.path.join(opts.storage , opts.query)

	if not os.path.exists(file):
		raise EntryError(f'Query `{opts.query}` does not exist in the storage')

	import _pickle

	cli_pass = False

	if opts.upass is None:

		cli_pass = True
		logger.info(f'Decrypting Entry `{opts.query}`')
		opts.upass = getpass("Enter User End Password: " , mask = (opts.clear == False) , colored = True)

	with open(file , 'rb') as filehandle:
		data = _pickle.load(filehandle)

	opts.upass = (opts.upass) if isinstance(opts.upass,bytes) else opts.upass.encode('utf8')

	if opts.sudo_mode:
		raise Exception("Currently Code in Progress")
		#TODO : Complete the code here

	else :

		result = None

		if cli_pass is True:

			for i in range(MAX_RETRIES):

				try :
					result = jihyo.dec_b2b(data['enc'] , opts.upass + data['salt'])
					break

				except jihyo.PasswordError:

					logger.critical(f'Incorrect User End Password Input. Please try again')
					opts.upass =  getpass("Enter User End Password: " , mask = (opts.clear == False) , colored = True).encode('utf8')
					continue

				except :
					raise

		else :

			result = jihyo.dec_b2b(data['enc'] , opts.upass + data['salt'])

	if result is None :

		raise ValueError(f'All retries exceeded. Please try the password entries once again')

	if opts.password_is_text:
		result = result.decode('utf8').rstrip('\0')
		#Logic to strip -> Can be padded password from old nint or otherwise

	if opts.driver == 'copy':
		import pyperclip
		pyperclip.copy(result)
		logger.info('The decrypted password has been copied to clipboard')

	elif opts.driver == 'print':

		logger.info('The decrypted password will be printed to the stream now')
		print(result , end = '')

	elif opts.driver == 'redis':

		from redis import Redis 
		
		Redis().set(opts.redis , result)
		logger.info(f'The decrypted password has been stored on Redis variable `{opts.redis}`')

	elif opts.driver == 'typewrite' :

		import time
		import pyautogui

		logger.info(f'[typewrite] Sleeping for {opts.typewrite[0]}s')
		time.sleep(opts.typewrite[0])

		logger.info("The decrypted password will be typewritten")
		pyautogui.typewrite(result , interval = opts.typewrite[-1])
		logger.info('The decrypted password has been typewritten')

	return None

def filtration(opts : Namespace):

	if opts.typewrite is not None :
		n = len(opts.typewrite)

		if n == 0 :

			opts.typewrite = DEFAULT_TYPE_OPTS

		else :
			if n == 1 :

				opts.typewrite = [ opts.typewrite[0] , DEFAULT_TYPE_OPTS[-1] ]

			elif n == 2 :

				pass

			elif n > 2  :
				opts.typewrite = opts.typewrite[:2]
				logger.warning(f'<Typewrite> driver can only have two arguments at max. Not more than that. Using first two arguments only')

		logger.info(f'Will be typing the password out. Time delay: {opts.typewrite[0]}s , Interval between clicks: {opts.typewrite[1]}s')

		opts.driver = "typewrite"

	elif opts.redis is not None:

		if opts.redis is True:
			opts.redis = REDIS_DEFAULT_VARIABLE

		opts.driver = 'redis'

	if opts.driver is None:
		opts.driver = DEFAULT_DRIVER

	if opts.sudo != False:

		if opts.sudo == True :

			opts.sudo = getpass("Enter sudo password: " , mask = (opts.clear == False) , colored = True )

		opts.sudo_mode = True

	else:
		opts.sudo_mode = False

	if opts.query is None :

		if opts.query_list is not None:
			opts.query = opts.query_list

	return opts

if __name__ == "__main__":


	import coloredlogs, pretty_traceback
	pretty_traceback.install()
	coloredlogs.install(fmt = "%(name)s %(asctime)s %(levelname)s : %(message)s" , level = logging.DEBUG)


	opts = filtration(arguments())
	DriverMain(opts)
