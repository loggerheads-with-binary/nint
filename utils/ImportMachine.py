import logging
from unittest.mock import DEFAULT 
logger = logging.getLogger('nint.import')

import os 
from . import exts 
import pandas as pd 
import sqlite3 
import questionary 
from .. import Errors
import json 
from .. import jihyocrypt as jihyo
 
functions = {   'take_consent' : None , 
                'table_name' : None , 
                "multiverify" : None , 
                'obtain_password' : None  , 
                'param_action' : 'ask' , 
                'salt_size' : 256//8}

DEFAULT_TABLE_NAMES =  ('nint' , 'nint_entries' , 'all_nint_entries')


def get_nint(cursor ):
    
    tables = cursor.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchall()
    
    if len(tables) == 0:
        raise ValueError("No tables found in database")
    
    if len(tables ) == 1:
        return tables[0][0]
    
    if len(tables) > 1:
        
        tables = {table[0] for table in tables}

        if functions['table_name'] in tables:
            return functions['table_name']  
        
        elif functions['table_name'] not in DEFAULT_TABLE_NAMES:
            logger.info(f'Table with name {functions["table_name"]} not found in database. Using defaults: {DEFAULT_TABLE_NAMES}')
        
        for table_name in DEFAULT_TABLE_NAMES:
            if table_name in tables:
                return table_name   
                
        else:
        
            logger.critical(f'No table with {DEFAULT_TABLE_NAMES} found in database. Probing user')
            
            return questionary.select('Which table from the database must be used?' , choices = tables).ask()

def file_import(file , extension):
    
    if extension in exts.CSV_EXTS:
        
        df = pd.read_csv(file , encoding = 'utf8')

    elif extension in exts.EXCEL_EXTS:
        
        df = pd.read_excel(file , encoding = 'utf8')
        
    elif extension in exts.SQL_EXTS:

        fileDB = sqlite3.connect(file)
        fileCursor = fileDB.cursor()
        table_name = get_nint(fileCursor)
        fileDB.close()
        
        df = pd.read_sql(table_name , file , encoding = 'utf8')
    
    elif extension in exts.JSON_EXTS:
        
        df = pd.read_json(file , encoding = 'utf8') 
    
    return df  
       
def __import(file, storage, jihyo_salt , dbfile , conn , DBCursor , import_mode : str = 'new'):
    
    #NOTE: Pre-processing
    
    logger.info("Commencing import operation")
    extension = os.path.splitext(file)[-1] 
    
    if extension not in exts.ALL_EXTS:
        raise NotImplementedError(f'Import has not been implemented yet for {extension}. Only {exts.ALL_EXTS} are allowed')
    
    df = file_import(file , extension)

    if import_mode  == 'new':
       
       DBCursor.executescript(os.path.join(os.path.dirname(__file__) , '..' , 'init.sql' , 'r' , encoding = 'utf8').read())
    
    if 'default_name' not in df.columns:
        
        logger.warning(f'`default_name` does not exist.')
        name_col = questionary.select("Kindly provide the column name with the default entrynames/ids" , choices = list(df.columns) + [None]).ask()
        
        if name_col is None:
            
            take_consent = functions['take_consent']
            
            if take_consent("Can we use the login part of any column? (Ex: picking `vicky.com` from `liz@vicky.com`"):
                
                name_col = questionary.select("Kindly provide the column name for said extraction" , choices = df.columns).ask()
                
                raise Errors.NotImplementedError()
                
    for val in ('pass' , 'password' ):
        if val in df.columns:
            to_encr_col = val 
    
    else:
        
        to_encr_col = questionary.select("Select Column with entry names to encrypt", choices = df.columns).ask()
    
    logger.info(f'Column with encryption passwords :: {to_encr_col} chosen')    
    
    for val in ('userpass' , 'upass' , 'userpassword' , 'user-password'):
        
        if val in df.columns:
            user_pass_col = val
            
    else:
        
        user_pass_col = questionary.select("Select Column with user passwords", choices = list(df.columns) + [None] ).ask()
    
    logger.info(f'User passwords column {user_pass_col} chosen')
    
    if user_pass_col is None:
        
        logger.warning(f'Passwords for each entry will be queried to the user')
        
    for val in ('alias' , 'aliases'):
        
        if val in df.columns:
            alias_col = val 
            
    else:
        alias_col = questionary.select("Select Column with entry aliases", choices = list(df.columns) + [None] ).ask()
 
    if alias_col is None:
        logger.warning(f'Implicitly presuming that there are no aliases to any entries')
        
    
    data_holder['default_name'].fillna()
        
    t =  list(set(df.columns).difference({to_encr_col , user_pass_col , alias_col , name_col }) )    
    if len(t) > 0 :
        
        logger.warning("Certain parameters other than the usually accepted ones exist in the given file")
        t = questionary.checkbox("Choose which of these parameters you'd like to use as parameters" , choices = t).ask()
        
    ##NOTE: Processing 
    
    assert df[to_encr_col].isnull().sum() == 0 , f'There are empty encryption passwords given'
    assert df[name_col].isnull().sum() == 0 , f'There are empty names given, which cannot be used by nint'
    
    if user_pass_col is not None:
        assert df[user_pass_col].isnull().sum() == 0 ,f'There are empty user-end passwords given'
    
    data_holder = pd.DataFrame({'default_name' : pd.Series(dtype = str) })
    aliases = pd.DataFrame({'alias' : pd.Series(dtype = str) , 'default_name' : pd.Series(dtype = str) })
    
    data_holder['default_name'] = df[name_col]
    aliases['default_name'] = df[name_col]
    aliases['alias'] = df[alias_col].apply(json.loads) 

    salt_length = functions['salt_size']
    data_holder['salt'] = data_holder[name_col].apply(lambda x : os.urandom(salt_length))
    
    if user_pass_col is None:
        data_holder['upass'] = np.nan
        user_pass_col = 'upass'
    
    data_holder['enc'] = data_holder[[to_encr_col , 'salt' , user_pass_col]].apply(lambda x , y, z  : make_enc(x,y,z))
    data_holder['sudo-enc'] = data_holder[[to_encr_col , 'salt' ]].apply(lambda x , y : make_sudo_enc(x , y, jihyo_salt))
                    
    ##Insert to DB 
    data_holder.to_sql('data_holder' , conn , if_exists = 'append' , index = False)
    
    ##Insert to DB 
    
    for alias_many , entry in zip(aliases['alias'] , aliases['default_name']):
        
        [DBCursor.execute('INSERT INTO aliases (alias , pk) VALUES (?, SELECT pk from data_holder WHERE default_name is ?)' , (alias , entry)) for alias in alias_many]
        
    ##Start to make files now 
    
def make_enc(to_encr , salt , upass ):
    
    if not isinstance(upass, bytes):
        upass = upass.encode('utf8')

        