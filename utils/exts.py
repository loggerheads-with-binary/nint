
CSV_EXTS = ('.csv' , )
EXCEL_EXTS = ('.xlsx' , '.xls' , '.xlsm')
SQL_EXTS = ('.db' , '.sqlite3' , '.sqlite' , '.dblite' )
JSON_EXTS = ('.json' , )
ALL_EXTS = CSV_EXTS + EXCEL_EXTS + SQL_EXTS + JSON_EXTS

DEFAULT_BLOCKSIZE = "200MB"
