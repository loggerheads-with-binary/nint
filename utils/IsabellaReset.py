
LARGE_VALID_EXTS = {'.csv'  , '.xlsx' , '.sqlite' , '.xlsm' , '.xls'}
__SMALL_VALID_EXTS = {'.yaml' , '.yml' , '.json' }

EXTS = LARGE_VALID_EXTS.union(__SMALL_VALID_EXTS )
del __SMALL_VALID_EXTS

