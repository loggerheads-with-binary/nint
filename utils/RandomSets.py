from ..config import RANDOM_SETS

class RandomSetGenerator():

    @staticmethod
    def alpha():

        import string 
        return list(string.ascii_letters)

    @staticmethod
    def num():

        return list('1234567890')

    @staticmethod
    def alphanum():

        return RandomSetGenerator.alpha() + RandomSetGenerator.num()

    @staticmethod
    def sym():

        import string 
        return list(string.punctuation)

    @staticmethod
    def all():

        return RandomSetGenerator.alpha() + RandomSetGenerator.num() + RandomSetGenerator.sym() 

    @staticmethod
    def numsym():

        return RandomSetGenerator.num() + RandomSetGenerator.sym()

    @staticmethod
    def alphasym(): 

        return RandomSetGenerator.alpha() + RandomSetGenerator.sym()


def validate(entry ):
    assert hasattr(RandomSetGenerator , entry), f'Method {entry} does not exist for Random Set generation. Please edit source file'

_ = [validate(entry) for entry in RANDOM_SETS]