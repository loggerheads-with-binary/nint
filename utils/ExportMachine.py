import dask.dataframe as dd
import pandas as pd 
import os 
from .. import jihyocrypt as jihyo 
import json 
import _pickle 
from pathlib import Path 

from .exts import CSV_EXTS, EXCEL_EXTS, SQL_EXTS, JSON_EXTS, ALL_EXTS


import logging 
logger = logging.getLogger('nint.export')

TABLE_NAME = 'nint'

def jinxloader(file , storage, sudopass : bytes , jihyo_salt : bytes ):

    file = os.path.join(storage , file) 
    
    if not  os.path.exists(file):
        return json.dumps(dict())

    with open(file , 'rb') as handle:

        data = _pickle.load(handle)

    sudo_enc , salt = data['sudo-enc'], data['salt']
    [data.pop(var, None) for var in ('salt' , 'sudo-enc' , 'enc')]  

    for val in (sudopass, jihyo_salt):
        assert isinstance(val , bytes) 

    password_major = jihyo_salt + salt

    return jihyo.dec_b2b(sudo_enc , password_major ) , json.dumps(data)

def csv_export(df  : dd , file : Path):

    return df.to_csv(file , encoding = 'utf-8' )

def excel_export(df : dd , file : Path, length):

    return df.to_excel(file , encoding = 'utf8' )

    """
    if length > 1048576 : 

        logger.warning(f'More than EXCEL_MAX_ROW_LIMIT submitted to the file. Exporting to Excel will be slow and will consist of multiple subsheets')


    temp = os.path.join(os.path.dirname(file ) , f'~{os.path.basename(file).csv}')
    csv_export(df , temp, encoding = 'utf8')

    ##Very risky operation 
    del df 

    from openpyxl import Workbook
    import csv 

    workbook = Workbook(file, {'constant_memory': True})

    nfiles = (length//1048576 + 1)
    logger.info(f'Excel file will consist of {length} entries and will be split into {nfiles} file(s)')

    worksheet = workbook.add_worksheet()

    with open(temp , 'r' , encoding = 'utf8') as handle:
        reader = csv.DictReader(handle)
        fields = reader.fieldnames

        worksheet.write_row(0 , 0 , fields)

        for row_counter , row in enumerate(reader , 1):
            worksheet.write_row( row_counter , 0 , [row[col] for col in fields])

            if ((row_counter -2) & 1048576 ) == 0 :

                worksheet = workbook.add_worksheet()
                worksheet.write_row(0 , 0 , fields)

    logger.info(f'Excel file successfully written')
    os.remove(temp)

    return nfiles  
    """

def final_export(df : dd , file : Path , extension = None , length : int = None ):    


    if extension is None:
        extension = os.path.splitext(file)[-1]

    if extension in CSV_EXTS:

        csv_export(df , file )

    elif extension in EXCEL_EXTS:

        excel_export(df , file , length)

    elif extension in JSON_EXTS:

        df.to_json(file , encoding = 'utf8')

    elif extension in SQL_EXTS: 

        import sqlite3 
        
        file = 'sqlite:///%s' %file

        logger.critical(f"By default configuration of nint, any existing table `nint` in `{file}` will be replaced entirely")
        df.to_sql(TABLE_NAME , file , if_exists = 'replace')
        logger.info("Update to database successful")

        return None 

def __export(file , sudopass, storage,  dbfile, jihyo_salt):

    extension = os.path.splitext(file)[-1]
    assert  extension in ALL_EXTS , f'File Extension `{os.path.splitext(file)}` is not supported. Only {ALL_EXTS} are supported'
    
    DB= 'sqlite:///{}'.format(dbfile.replace('\\' , '/'))

    dholder = pd.read_sql_table('data_holder' , DB )
    aliases = pd.read_sql_table('aliases' , DB, )
    logger.info("Setup aliases and names tables")

    print(dholder.columns , aliases.columns)
    
    dholder['aliases'] = dholder['pk'].apply(lambda x : aliases[aliases['pk'] == x]['alias'].to_json(orient = 'values') )
    del aliases 
    logger.info("Processed aliases successfully")

    dholder.drop('pk' , axis = 1 , inplace = True)
    dholder.drop('sha256' , axis = 1 , inplace = True)

    ##Password, params json and aliases json
    assert isinstance(jihyo_salt , bytes) , f'Internal Error::jihyo_salt is not bytes'
    
    dholder['password'] , dholder['params'] = zip(*dholder['default_name'].apply(lambda x : 
                                                    jinxloader(x , storage , sudopass, jihyo_salt )))
    logger.info("Setup passwords and other parameters successfuly")

    ##Final Export 

    ##actually run something 
    #logger.info("Computing Dask Dataframe")
    length = dholder.shape[0] 

    final_export(dholder , file  , extension, length )
    logger.info(f"Successfully completed export to `{file}`")
