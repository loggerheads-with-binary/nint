import sqlite3 
from pathlib import Path
import shutil  
from .. import config as cfg  
from .basicutils import make_link 
import os 
import time 

import logging 
logger = logging.getLogger('nint.backup')

def ind_entry(storage : Path , backup_dir : Path , name : str , pk   , DBCursor : sqlite3.Cursor):

    aliases = DBCursor.execute('SELECT alias from aliases WHERE pk is ?' , (pk , ))
    aliases = aliases.fetchall()
    aliases = {alias['alias'] for alias in aliases}


    logger.info(f'Backing up entry {name} with aliases {aliases}')

    xfile = os.path.join(backup_dir , name)

    if not os.path.exists(os.path.dirname(xfile)):
        os.makedirs(xfile)
    
    shutil.copy(os.path.join(storage , name) , xfile)
    
    for alias in aliases:

        aliasfile = os.path.join(backup_dir , alias)
        aliasdir = os.path.dirname(aliasfile)
        if not os.path.exists(aliasdir):
            os.makedirs(aliasdir)

        make_link(source = aliasfile, target = xfile)

    logger.info(f'Backup for entry {name} successfully completed')

def backup(storage : Path , backup_dir  : Path , DB : sqlite3.Connection , DBCursor : sqlite3.Cursor):
    
    assert os.path.isdir(storage), f'Directory to copy from does not exist'
    assert os.path.isfile(os.path.join(storage , cfg.DATABASE_NAME)) , f'Database does not exist'
    
    s_time = time.time()

    if not os.path.isdir(backup_dir):
        if os.path.exists(backup_dir):
            raise RuntimeError('Backup directory is already the name of an existing file')
    
        os.makedirs(backup_dir)
    else:

        
        shutil.rmtree(backup_dir)
        os.makedirs(backup_dir)

    shutil.copy(os.path.join(storage, cfg.DATABASE_NAME ) , os.path.join(backup_dir, cfg.DATABASE_NAME)) 
    cursor2 = DB.cursor() 
    machinery = DBCursor.execute('SELECT * from data_holder;')
   
    counter = len([ind_entry(storage, backup_dir , entry['default_name'] , entry['pk'] , cursor2) for entry in machinery])
    e_time = time.time()
   
    logger.info(f'Backed up total {counter} entries in {e_time - s_time} seconds. Average {(e_time-s_time)/counter} secs per entry')
    return counter 