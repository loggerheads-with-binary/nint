import os
from .. import Errors 
import os, sys, subprocess


import logging 
logger = logging.getLogger('nint-utils')

def get_pk(entry : str, DBCursor ,   get_default_name : bool = True  ):

    query = "SELECT * from data_holder WHERE default_name is ?"
    
    s = DBCursor.execute(query , (entry , )).fetchone()

    if not s:
        logger.error(f'No entry with default_name `{entry}` exists. Will check as possible alias')

        query = 'SELECT pk from aliases WHERE alias is ?'
        s2 = DBCursor.execute(query , (entry , )).fetchone()

        if not s2:
            
            raise Errors.InexistentEntry(f'No entry found with name/alias {entry}')

        if get_default_name:
            query = "SELECT * from data_holder where pk is ?"
            s = DBCursor.execute(query , (s2['pk'] ,  )).fetchone()
            return s['pk'] , s['default_name']

        return s['pk']

    if get_default_name:
        return s['pk'], s['default_name']
    
    return s['pk']

def make_link(source , target):

    source = os.path.abspath(source)
    target = os.path.abspath(target)

    logger.info(f'Will hardlink {source} to {target}')

    if os.name == 'nt':

        return bool(subprocess.call(f'mklink /h "{source}" "{target}"' , shell = True ) == 0 )

    elif os.name == 'posix':

        return bool(subprocess.call(['ln' , target , source] ) == 0 )

    else:

        raise PlatformError( 'Unsupported platform. Symlinking is only allowed in NT and POSIX clients. Not for operating system {os.name}')

def make_dict(params : list):

    r = dict()

    for entry in params:

        c = entry.count('=')

        if c==1:

            _tmp = entry.split('=')
            r.update({_tmp[0] : _tmp[1]})

        if  c== 0:
            raise ValueError(f'Parameter {entry} cannot be parsed since it does not follow key=value syntax')

        if c > 1 :
            #raise ValueError(f'Parameter {entry} cannot be parsed due to ambiguous key/value names')
            _tmp = entry.split('=')
            key , value = _tmp[0] , '='.join(_tmp[1:])
            logger.warning(f'Parameter {entry} will be parsed as `{key}`:`{value}`')
            r.update({key : value })

    return r 

import os, sys, subprocess

def startfile(filename):
    
    if sys.platform == "win32":
        os.startfile(filename)
    
    else:
        opener = "open" if sys.platform == "darwin" else "xdg-open"
        subprocess.call([opener, filename])

def take_consent(prompt , choices = {'y' : True , 'n' : False} , tries : int = 4, pc : bool = False):

    if pc is True:
        return True 

    prompt = f'{prompt}[{"/".join(choices.keys())}] '

    for i in range(tries):

        answer = input(prompt).strip().lower()

        if answer not in choices.keys():
            logger.warning(f'Invalid input. Please try again')
            continue 

        return choices[answer]

    return False 
