from enum import Enum 
import os, sys 



PROG_PATH = os.path.dirname(os.path.abspath(__file__))
DEFAULT_STORAGE_PATH = os.path.join(PROG_PATH, '.n4_storage')
DEFAULT_SQL_SCRIPT = os.path.join(PROG_PATH , "init.sql")

__version__ = '5.0.0'
HELP_TEXT = os.path.join(PROG_PATH, 'help.md')
DATABASE_NAME = '.NINT_STANDARD_DATA_STORAGE.sqlite'


diceware_flags = lambda n = 8 , sep = '-', s = 2 :  {'sep' : sep , 'n' : n , 'special' : s}


Actions = Enum("ACTIONS" , """VERSION USAGE BLANKFILE 
                            GENERATE RETRIEVE 
                            UPDATE MODIFY RENAME
                            SHOW REMOVE LIST 
                            ADD_ALIAS RM_ALIAS 
                            IMPORT EXPORT BACKUP 
                            HARD_RESET SOFT_RESET JADE_RESET""")

PAD_LENGTH = 32             ##Default salt length in bytes 
SALT_LENGTH = 256//8        #Bytes 
SUDO_SALT_LENGTH = 2048//8  #Bytes 
DEFAULT_RANDOM_LENGTH = 64 
COLORED = True 
UNICODE_SEGFAULT_IGNORE = True 
CLEAR_VAR = False 

Drivers = Enum("DRIVERS" , 'COPY PRINT TYPEWRITE REDIS FILE')
DEFAULT_DRIVER = Drivers.COPY           ##Set None to ensure default driver is asked for 
TYPEWRITER_DEFAULTS = (4 , 0.05)
REDIS_DEFAULT = 'nint'

Password_Means = Enum("PMEANS" , "FILE TEXT DICEWARE RANDOM")

MAX_PASS_TRIES = 3 
MAX_SUDO_TRIES = 3 

RANDOM_SETS = ('alpha' , 'num' , 'alphanum' , 'all' , 'sym' ,  'numsym' , 'alphasym')  
RandomSets = Enum('RandomSets' , ' '.join(RANDOM_SETS))
IMPORT_MODE = Enum("IMPORT_MODE" , "NEW APPEND")