CREATE TABLE sudo (

    name TEXT PRIMARY KEY NOT NULL,
    value image NOT NULL 
);

CREATE TABLE data_holder (

    pk INTEGER PRIMARY KEY,
    default_name TEXT NOT NULL,
    sha256 VARBINARY(32) NOT NULL,

    CONSTRAINT unique_f_names UNIQUE(default_name)

);

CREATE TABLE aliases (

    pk INTEGER, 
    alias TEXT NOT NULL,  


    CONSTRAINT unique_alias_names UNIQUE(alias),
    FOREIGN KEY(pk) REFERENCES DATA_HOLDER(pk)
);